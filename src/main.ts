import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "@/store";
import "./plugins/element.js";
import "bootstrap";
import "./assets/css/all.css";
import "./assets/css/bootstrap.min.css";
import "./assets/css/custom.css";
import Notifications from "vue-notification";
import {
  BreadcrumbPlugin,
  ModalPlugin,
  FormCheckboxPlugin,
  FormGroupPlugin,
  TooltipPlugin,
  BSpinner
} from "bootstrap-vue";

// Require Froala Language js file.
require("froala-editor/js/languages/es.js");

// Require Froala Editor js file.
require("froala-editor/js/froala_editor.pkgd.min.js");

// Require Froala Editor css files.
require("froala-editor/css/froala_editor.pkgd.min.css");
require("froala-editor/css/froala_style.min.css");

// Require Froala Editor Code View files
require("froala-editor/js/plugins/code_view.min.js");
require("froala-editor/js/plugins/code_beautifier.min.js");
require("froala-editor/css/plugins/code_view.min.css");

// Require Froala Editor Fullscreen files
require("froala-editor/js/plugins/fullscreen.min.js");
require("froala-editor/css/plugins/fullscreen.min.css");

// Require Froala Editor component files
require("froala-editor/js/plugins/url.min.js");
require("froala-editor/js/plugins/video.min.js");
require("froala-editor/css/plugins/video.min.css");
require("froala-editor/js/plugins/table.min.js");
require("froala-editor/css/plugins/table.min.css");
require("froala-editor/js/plugins/colors.min.js");
require("froala-editor/css/plugins/colors.min.css");
require("froala-editor/js/plugins/draggable.min.js");
require("froala-editor/css/plugins/draggable.min.css");
require("froala-editor/js/plugins/font_family.min.js");
require("froala-editor/js/plugins/font_size.min.js");
require("froala-editor/js/plugins/link.min.js");
require("froala-editor/js/plugins/lists.min.js");
require("froala-editor/js/plugins/paragraph_format.min.js");
require("froala-editor/js/plugins/paragraph_style.min.js");

// Import and use Vue Froala lib.
import VueFroala from "vue-froala-wysiwyg";

export const serverBus = new Vue();

Vue.use(VueFroala);
Vue.use(Notifications);
Vue.use(ModalPlugin);
Vue.component("b-spinner", BSpinner);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
