import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import AdminBack from "@/api/AdminBack";

Vue.use(Router);

const ifNotAuthenticated = (to: any, from: any, next: any) => {
  if (!AdminBack.session.isAuthenticated()) {
    next();
    return;
  }
  next("/");
};

const ifAuthenticated = async (to: any, from: any, next: any) => {
  if (AdminBack.session.isAuthenticated()) {
    await AdminBack.setupLocalStates();
    next();
    return;
  }
  next("/login");
};

export default new Router({
  routes: [
    {
      path: "/login",
      name: "login",
      beforeEnter: ifNotAuthenticated,
      component: () => import("./views/Login.vue")
    },
    {
      path: "/",
      name: "home",
      component: Home,
      redirect: "/card",
      beforeEnter: ifAuthenticated,
      children: [
        {
          path: "/card",
          name: "card",
          component: () => import("./views/Card.vue")
        },
        {
          path: "/report",
          name: "report",
          component: () => import("./views/Report.vue")
        },
        {
          path: "/page",
          name: "page",
          component: () => import("./views/Page.vue")
        },
        {
          path: "/user",
          name: "user",
          component: () => import("./views/User.vue")
        },
        {
          path: "/me/password",
          name: "password",
          component: () => import("./views/Password.vue")
        }
      ]
    }
  ]
});
