const getters = {
  landingURL: (state: any) => state.landingURL,
  landings: (state: any) => state.landings
};

export default getters;
