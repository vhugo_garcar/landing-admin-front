const mutations = {
  updateLandingURL: (state: any, landingURL: string) => {
    state.landingURL = landingURL;
  },
  updateLandings: (state: any, landings: object) => {
    state.landings = landings;
  }
};

export default mutations;
