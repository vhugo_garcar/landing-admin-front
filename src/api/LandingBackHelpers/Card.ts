import * as LandingBack from "../LandingBack";

const ACTIONS = {
  CARD: "/card/",
  CARD_ITEM: "/card/{id}/",
  CARD_ORDER: "/card/order/"
};

export function find(params?: object) {
  return LandingBack.API().get(ACTIONS.CARD, { params });
}

export function findOne(id: string) {
  return LandingBack.API().get(ACTIONS.CARD_ITEM.replace("{id}", id));
}

export function create(body: object) {
  return LandingBack.API().post(ACTIONS.CARD, body);
}

export function update(id: string, body: object) {
  return LandingBack.API().patch(ACTIONS.CARD_ITEM.replace("{id}", id), body);
}

export function order(body: object) {
  return LandingBack.API().post(ACTIONS.CARD_ORDER, body);
}
