import * as LandingBack from "../LandingBack";

const ACTIONS = {
  PAGE: "/page/",
  PAGE_ITEM: "/page/{id}/"
};

export function find(params?: object) {
  return LandingBack.API().get(ACTIONS.PAGE, { params });
}

export function findOne(id: string) {
  return LandingBack.API().get(ACTIONS.PAGE_ITEM.replace("{id}", id));
}

export function create(body: object) {
  return LandingBack.API().post(ACTIONS.PAGE, body);
}

export function update(id: string, body: object) {
  return LandingBack.API().patch(ACTIONS.PAGE_ITEM.replace("{id}", id), body);
}
