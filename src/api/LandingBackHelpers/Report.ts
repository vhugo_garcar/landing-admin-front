import * as LandingBack from "../LandingBack";

const ACTIONS = {
  REPORT: "/report/",
  REPORT_ITEM: "/report/{id}/",
  REPORT_RESEND: "/report/{id}/resend",
  REPORT_COLUMNS: "/report/columns"
};

export function find(params?: object) {
  return LandingBack.API().get(ACTIONS.REPORT, { params });
}

export function findOne(id: string) {
  return LandingBack.API().get(ACTIONS.REPORT_ITEM.replace("{id}", id));
}

export function create(body: object) {
  return LandingBack.API().post(ACTIONS.REPORT, body);
}

export function update(id: string, body: object) {
  return LandingBack.API().patch(ACTIONS.REPORT_ITEM.replace("{id}", id), body);
}

export function resend(id: string, body?: object) {
  return LandingBack.API().post(
    ACTIONS.REPORT_RESEND.replace("{id}", id),
    body
  );
}

export function columns(params?: object) {
  return LandingBack.API().get(ACTIONS.REPORT_COLUMNS, { params });
}
