import AdminBack from "../AdminBack";

const ACTIONS = {
  USER: "/user/",
  USER_ITEM: "/user/{id}/"
};

export function find(params?: object) {
  return AdminBack.API.get(ACTIONS.USER, { params });
}

export function findOne(id: string) {
  return AdminBack.API.get(ACTIONS.USER_ITEM.replace("{id}", id));
}

export function create(body: object) {
  return AdminBack.API.post(ACTIONS.USER, body);
}

export function update(id: string, body: object) {
  return AdminBack.API.patch(ACTIONS.USER_ITEM.replace("{id}", id), body);
}
