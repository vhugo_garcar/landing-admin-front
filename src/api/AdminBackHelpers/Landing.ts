import AdminBack from "../AdminBack";

const ACTIONS = {
  LANDING: "/system/",
  LANDING_ITEM: "/system/{id}/"
};

export function find(params?: object) {
  return AdminBack.API.get(ACTIONS.LANDING, { params });
}

export function findOne(id: string) {
  return AdminBack.API.get(ACTIONS.LANDING_ITEM.replace("{id}", id));
}

export function create(body: object) {
  return AdminBack.API.post(ACTIONS.LANDING, body);
}

export function update(id: string, body: object) {
  return AdminBack.API.patch(ACTIONS.LANDING_ITEM.replace("{id}", id), body);
}
