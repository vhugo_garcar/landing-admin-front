import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";

Vue.use(VueAxios, axios);

const AUTHORIZATION = "authorization";
const ACCESS_TOKEN = "token";
const REFRESH_TOKEN = "refresh";
const USER = "user";

let _IS_REFRESHING_TOKEN = false;
let _refreshSubscribers: Array<any> = [];

export default class AdminBack {
  private static BASE: string = process.env.VUE_APP_BACK;
  static API = axios.create({
    baseURL: AdminBack.BASE
  });

  private static ACTIONS = {
    LOGIN: "/login",
    REFRESH: "/refresh",
    ME: "/me"
  };

  static IS_USER_INFO_FETCHED = false;
  private static _interceptor = -1;

  static state = {
    token: () => localStorage.getItem(ACCESS_TOKEN) || "",
    refresh: () => localStorage.getItem(REFRESH_TOKEN) || ""
  };

  static session = {
    isAuthenticated: () => !!AdminBack.state.token(),
    user: () => JSON.parse(sessionStorage.getItem(USER) || "{}")
  };

  static async login(body: object) {
    return AdminBack.API.post(AdminBack.ACTIONS.LOGIN, body)
      .then(resp => {
        const data = resp.data.data;
        const token = data.auth[ACCESS_TOKEN];
        const refresh = data.auth[REFRESH_TOKEN];
        localStorage.setItem(ACCESS_TOKEN, token);
        localStorage.setItem(REFRESH_TOKEN, refresh);
        return AdminBack.setupLocalStates();
      })
      .catch(err => {
        AdminBack.destroySession();
        throw err;
      });
  }

  static destroySession() {
    AdminBack.IS_USER_INFO_FETCHED = false;
    localStorage.clear();
    sessionStorage.clear();
    AdminBack.API.interceptors.response.eject(AdminBack._interceptor);
    AdminBack._interceptor = -1;
    delete AdminBack.API.defaults.headers.common[AUTHORIZATION];
    // this.$emit("sessionDestroyed");
  }

  static async setupLocalStates() {
    AdminBack.API.interceptors.request.use(resp => {
      const token = localStorage.getItem(ACCESS_TOKEN);
      resp.headers[AUTHORIZATION] = token;
      return resp;
    });
    AdminBack._interceptor = AdminBack.API.interceptors.response.use(
      resp => resp,
      AdminBack.refresh
    );

    const res = await AdminBack.API.get(AdminBack.ACTIONS.ME);
    const me = res.data.data;

    sessionStorage.setItem(USER, JSON.stringify(me));

    AdminBack.IS_USER_INFO_FETCHED = true;
  }

  static refresh(error: any) {
    const originalRequest = error.config;
    const isAuth = originalRequest.url.includes(AdminBack.ACTIONS.LOGIN);
    if (error.response.status === 401 && !isAuth) {
      delete originalRequest.headers[AUTHORIZATION];
      delete Vue.axios.defaults.headers[AUTHORIZATION];
      originalRequest._retry = true;
      if (!_IS_REFRESHING_TOKEN) {
        _IS_REFRESHING_TOKEN = true;
        Vue.axios
          .post(AdminBack.BASE + AdminBack.ACTIONS.REFRESH, {
            refresh: AdminBack.state.refresh()
          })
          .then((resp: any) => {
            const data = resp.data.data;
            const token = data.auth[ACCESS_TOKEN];
            const refresh = data.auth[REFRESH_TOKEN];
            localStorage.setItem(ACCESS_TOKEN, token);
            localStorage.setItem(REFRESH_TOKEN, refresh);

            _IS_REFRESHING_TOKEN = false;
            AdminBack._onTokenRefreshed(token);
          })
          .catch((err: any) => {
            AdminBack.destroySession();
          });
      }

      const retryOrigReq = new Promise(resolve => {
        AdminBack._subscribeTokenRefresh((token: string) => {
          originalRequest.headers[AUTHORIZATION] = token;
          resolve(axios(originalRequest));
        });
      });
      return retryOrigReq;
    } else {
      return Promise.reject(error);
    }
  }

  static logout() {
    return AdminBack.API.delete(AdminBack.ACTIONS.LOGIN)
      .then(() => {
        AdminBack.destroySession();
        return Promise.resolve();
      })
      .catch(error => {
        if (error.response.status == 401) {
          AdminBack.destroySession();
          return Promise.resolve();
        }
        throw error;
      });
  }

  private static _onTokenRefreshed(token: any) {
    _refreshSubscribers = _refreshSubscribers.filter((cb: any) => cb(token));
  }

  private static _subscribeTokenRefresh(cb: any) {
    _refreshSubscribers.push(cb);
  }
}
