import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";

Vue.use(VueAxios, axios);

export function API() {
  return axios.create({
    baseURL: localStorage.getItem("landingURL") || ""
  });
}
