export default class Landing {
  id?: number;
  name: string = "";
  url: string = "";
  is_active: boolean = true;
  createdAt: any = null;
  updatedAt: any = null;
}
