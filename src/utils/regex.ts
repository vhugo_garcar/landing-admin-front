/**
 * @description A class which store all RegExp
 * static methods.
 */

export default class Regex {
  public static names: RegExp = /^[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ ][a-zA-ZÁÉÍÓÚáéíóúÜüñÑ -']*[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ ]$/;
  public static surname: RegExp = /^(:?[dD][eE](:? [a-km-zA-KM-ZÁÉÍÓÚáéíóúÜüñÑ]|(:?[lL]| [lL])(:?[aAoO]|[aAoO][sS]|) [a-zA-ZÁÉÍÓÚáéíóúÜüñÑ]+|[lL](:?[aAoO][a-rt-zA-RT-ZÁÉÍÓÚáéíóúÜüñÑ]|[aAoO][sS][a-zA-ZÁÉÍÓÚáéíóúÜüñÑ]))(:?[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ][a-zA-ZÁÉÍÓÚáéíóúÜüñÑ -]+[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ ]|)|[dD](:?[a-dA-D]|[f-zF-ZÁÉÍÓÚáéíóúÜüñÑ])[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ]|[dD](:?[a-dA-D]|[f-zF-ZÁÉÍÓÚáéíóúÜüñÑ])[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ][a-zA-ZÁÉÍÓÚáéíóúÜüñÑ -]*[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ ]|(:?[a-cA-C]|[e-zE-ZÁÉÍÓÚáéíóúÜüñÑ])[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ][a-zA-ZÁÉÍÓÚáéíóúÜüñÑ -]*[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ]|[dD][eE][a-zA-ZÁÉÍÓÚáéíóúÜüñÑ](?:[aAoO][sS][a-zA-ZÁÉÍÓÚáéíóúÜüñÑ]|[b-np-zB-NP-ZÁÉÍÓÚáéíóúÜüñÑ])[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ]+|[dD][eE] [lL][b-np-zB-NP-ZÁÉÍÓÚáéíóúÜüñÑ][a-rt-zA-RT-ZÁÉÍÓÚáéíóúÜüñÑ-]+[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ ]*|[a-ce-zA-CE-ZÁÉÍÓÚáéíóúÜüñÑ][a-zA-ZÁÉÍÓÚáéíóúÜüñÑ]+|[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ]{2}[a-km-zA-KM-ZÁÉÍÓÚáéíóúÜüñÑ][a-zA-ZÁÉÍÓÚáéíóúÜüñÑ]*|[yY] [a-zA-ZÁÉÍÓÚáéíóúÜüñÑ]{2}[a-zA-ZÁÉÍÓÚáéíóúÜüñÑ]+)$/;
  public static email: RegExp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public static emails: RegExp = /^((([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))[ ]*[,]*[ ]*)+$/;
  public static phone: RegExp = /^[+]?[(]?[0-9]{3}[)]?[-\s.]?[0-9]{3}[-\s.]?[0-9]{4,6}$/;
  public static alphanumeric: RegExp = /^[a-zA-Z0-9ÁÉÍÓÚáéíóúÜüñÑ \-&!?¡¿+.,]{4,}$/;
  public static list: RegExp = /^(.{4,}\n*)+$/;
  public static url: RegExp = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([-.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
  public static password: RegExp = /^.{6,}$/;

  public static validate(regex: string, text: string) {
    return (Regex as any)[regex].test(text);
  }

  public static toString(regex: string) {
    return (Regex as any)[regex]
      .toString()
      .substring(1)
      .slice(0, -1);
  }
}
