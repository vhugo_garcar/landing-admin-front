let forge = require("node-forge");

export default class Security {
  static encrypt(password: string, pem: string): string {
    let publicKey = forge.pki.publicKeyFromPem(pem);
    let encrypted = publicKey.encrypt(this.b64ToUnicode(password), "RSA-OAEP");
    return forge.util.encode64(encrypted);
  }

  static b64ToUnicode(password: string) {
    return unescape(encodeURIComponent(password));
  }
}
