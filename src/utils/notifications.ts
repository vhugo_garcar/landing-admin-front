import Vue from "vue";

/**
 * @description A class which makes a little wrapper for vue-notifications into
 * static methods.
 */
export default class Notify {
  static get ERROR(): string {
    return "error";
  }
  static get WARNING(): string {
    return "warn";
  }
  static get SUCCESS(): string {
    return "success";
  }
  static get INFO(): string {
    return "info";
  }

  static show = (text: string, type: string, title?: string) =>
    Vue.notify({ type, text, title });
  static successful = (text: string, title?: string) =>
    Vue.notify({ type: Notify.SUCCESS, text, title });
  static error = (text: string, title?: string) => {
    if (!text) text = "Hubo un problema al conectar con el servicio";
    Vue.notify({ type: Notify.ERROR, text, title });
  };
  static info = (text: string, title?: string) =>
    Vue.notify({ type: Notify.INFO, text, title });
  static warning = (text: string, title?: string) =>
    Vue.notify({ type: Notify.WARNING, text, title });

  /**
   * @description A method which handles notifications when Geb response is
   * between 300 and 521.
   * @param error The error thrown in axios.
   * @returns {Void}
   */
  static gebServerError = (error: any) => {
    console.log(error);

    if (!error.response || error.response.status >= 500)
      return Notify.error("Hubo un problema al conectar con el servicio");

    const { message, data } = error.response.data;
    if (!Object.keys(data).length) return Notify.error(message);

    let err: Array<string> = [];
    for (const k in data) err = err.concat(data[k]);
    Notify.error(err.join("\n"), message);
  };
}
